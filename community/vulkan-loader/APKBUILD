# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=vulkan-loader
pkgver=1.2.169
pkgrel=0
arch="all"
url="https://www.khronos.org/vulkan"
pkgdesc="Vulkan Installable Client Driver (ICD) Loader"
license="Apache-2.0"
depends_dev="vulkan-headers"
makedepends="$depends_dev
	cmake
	libx11-dev
	libxrandr-dev
	python3
	wayland-dev
	"
source="https://github.com/khronosgroup/vulkan-loader/archive/v$pkgver/vulkan-loader-v$pkgver.tar.gz"
subpackages="$pkgname-dev"
options="!check" # No tests
builddir="$srcdir/Vulkan-Loader-$pkgver"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DCMAKE_INSTALL_SYSCONFDIR=/etc \
		-DCMAKE_INSTALL_DATADIR=share \
		-DCMAKE_SKIP_RPATH=True \
		-DVULKAN_HEADERS_INSTALL_DIR=/usr
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="6b171ac75457d5430b7ffd71214009f97bdf6571206400ff142599b213ce08a8932b5cb52ae8ae87ef84db68aab1a27c511b3c9ea7756699d7383be52c43cb98  vulkan-loader-v1.2.169.tar.gz"
