# Contributor: Nick Black <dankamongmen@gmail.com>
# Maintainer: Nick Black <dankamongmen@gmail.com>
pkgname=notcurses
pkgver=2.2.1
pkgrel=0
pkgdesc="Blingful character graphics and TUI library"
url="https://nick-black.com/dankwiki/index.php/Notcurses"
arch="all"
license="Apache-2.0"
makedepends="cmake ncurses-dev linux-headers ffmpeg-dev doctest-dev libunistring-dev readline-dev"
subpackages="$pkgname-static $pkgname-dbg $pkgname-dev $pkgname-doc
	$pkgname-libs $pkgname-demo ncneofetch ncls $pkgname-view $pkgname-tetris"
source="$pkgname-$pkgver.tar.gz::https://github.com/dankamongmen/notcurses/archive/v$pkgver.tar.gz"

# Tests fail on x86_64, upstream is aware https://github.com/dankamongmen/notcurses/issues/1344
case "$CARCH" in
	x86_64) options="!check" ;;
esac

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake -B build \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=None \
		-DUSE_PANDOC=off \
		-DUSE_QRCODEGEN=off \
		-DUSE_PYTHON=off \
		$CMAKE_CROSSOPTS
	make -C build
}

check() {
	make -C build test
}

package() {
	make -C build DESTDIR="$pkgdir" install
}

libs() {
	amove usr/lib/libnotcurses*.so.*
}

demo() {
	amove usr/bin/notcurses-demo
	amove usr/bin/notcurses-tester
	amove usr/share/notcurses
}

ncneofetch() {
	amove usr/bin/ncneofetch
}

ncls() {
	amove usr/bin/ncls
}

view() {
	amove usr/bin/ncplayer
}

tetris() {
	amove usr/bin/nctetris
}

sha512sums="041a16e4174ef3ebee1077fe4eaed7136c5039a935886216c2d4840332b27d073aa83876697413c83c4f4ed432edf34b44a0d6a71592fbfb1e476ccf86ded269  notcurses-2.2.1.tar.gz"
